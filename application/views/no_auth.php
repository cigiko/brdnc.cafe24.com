		<div class="container" style="text-align: center; padding-top: 50px; <?php if( !is_mobile()) echo 'height: 620px;'?>">
			<div class="page-header">
				<h3>해당 페이지에 대한 조회 권한이 없습니다.</h3>
			</div>
			<p class="lead">관리자(010-3320-0088)에게 문의하여 주십시요!</p>
			<p>또는 <a href="javascript:message_win('')" class="no_auth">관리자나 해당 직원에게 메세지</a>를 보낼 수 있습니다.</p>
		</div>