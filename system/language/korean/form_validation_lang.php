<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2016, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (https://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2016, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['form_validation_required']		= '{field} 은(는) 필수 입력 항목입니다.';
$lang['form_validation_isset']			= '{field} 은(는) 반드시 필요한 값입니다.';
$lang['form_validation_valid_email']		= '유효한 {field} 주소를 입력해 주세요.';
$lang['form_validation_valid_emails']		= '{field} 은(는) 모두 유효한 이메일 주소를 입력해 주세요.';
$lang['form_validation_valid_url']		= '{field} 은(는) 유효한 URL을 포함해야 합니다.';
$lang['form_validation_valid_ip']		= '{field} 은(는) 유효한 IP를 포함해야 합니다.';
$lang['form_validation_min_length']		= '{field} 은(는) 최소 {param} 자 이상의 문자를 입력하세요.';
$lang['form_validation_max_length']		= '{field} 은(는) 최대 {param} 자 이하의 문자를 입력하세요.';
$lang['form_validation_exact_length']		= '{field} 은(는) 정확히 {param} 자의 문자를 입력하세요.';
$lang['form_validation_alpha']			= '{field} 은(는) 알파벳 문자만 포함할 수 있습니다.';
$lang['form_validation_alpha_numeric']		= '{field} 은(는) 알파벳과 숫자만 포함할 수 있습니다.';
$lang['form_validation_alpha_numeric_spaces']	= '{field} 은(는) 알파벳과 숫자, 공백문자만 포함할 수있습니다.';
$lang['form_validation_alpha_dash']		= '{field} 은(는) 알파벳과 숫자, 밑줄, 대시문자만 입력할 수 있습니다.';
$lang['form_validation_numeric']		= '{field} 은(는) 숫자만 입력할 수 있습니다.';
$lang['form_validation_is_numeric']		= '{field} 은(는) 숫자(문자열 포함)만 입력할 수 있습니다.';
$lang['form_validation_integer']		= '{field} 은(는) 반드시 정수만 입력할 수 있습니다.';
$lang['form_validation_regex_match']		= '{field}의 형식이 일치하지 않습니다.';
$lang['form_validation_matches']		= '{field} 이(가) {param} 필드와 일치하지 않습니다.';
$lang['form_validation_differs']		= '{field} 은(는) {param} 필드와 값이 달라야 합니다.';
$lang['form_validation_is_unique'] 		= '{field} 은(는) 반드시 유일(unique)한 값이 포함되어야 합니다.';
$lang['form_validation_is_natural']		= '{field} 은(는) 반드시 0과 자연수만 입력하여야 합니다.';
$lang['form_validation_is_natural_no_zero']	= '{field} 은(는) 반드시 0을 초과하는 자연수만 입력하여야 합니다.';
$lang['form_validation_decimal']		= '{field} 은(는) 반드시 십진수가 포함되어야 합니다.';
$lang['form_validation_less_than']		= '{field} 은(는) 반드시 {param} 필드보다 큰 값이 입력되어야 합니다.';
$lang['form_validation_less_than_equal_to']	= '{field} 은(는) 반드시 {param} 필드보다 같거나 큰 값이 입력되어야 합니다.';
$lang['form_validation_greater_than']		= '{field} 은(는) 반드시 {param} 필드보다 작은 값이 입력되어야 합니다.';
$lang['form_validation_greater_than_equal_to']	= '{field} 은(는) 반드시 {param} 필드보다 같거나 작은 값이 입력되어야 합니다.';
$lang['form_validation_error_message_not_set']	= '{field} 필드에 지정된 다음 에러메세지를 불러올 수 없습니다.';
$lang['form_validation_in_list']		= '{field} 은(는) 반드시 {param} 필드 중에 하나이어야 합니다.';
